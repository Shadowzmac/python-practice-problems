# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# i need to verify if the inputs in word are the same forwards and back

def is_palindrome(word):
    reversed_list = reversed(word)
    reversed_word = "".join(reversed_list)
    if reversed_word == word:
        return True
    else:
        return False


name1 = is_palindrome("race")
print(name1)