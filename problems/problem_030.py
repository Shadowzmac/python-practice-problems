# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    max_val = values[0]
    second_max = None
    for value in values[1:]:
        if value > max_val:
            second_max = max_val
            max_val = value
        elif second_max is None or value > second_max:
            second_max = value
    return second_max
